Ser� constru�do um sistema para o controle remoto de fun��es relacionadas a gest�o de acesso, ilumina��o e monitoramento:

1. Possibilidade de adicionar at� 4 c�meras ao sistema, as quais poder�o ser monitoradas de forma
remota por um aplicativo para smartphones, bem como uma interface web;
2. Ser� poss�vel � implementa��o de at� duas fechaduras eletr�nicas, as quais poder�o ser desarmadas
com senhas num�ricas ou identifica��o biom�trica.
3. Possibilidade de acionamento de cargas de ilumina��o para auxiliar no monitoramento remoto quando
o ambiente se encontrar escuro, bem como o agendamento dessas cargas para quando realizar-se
viagens se tenha a impress�o que existem pessoas na casa.